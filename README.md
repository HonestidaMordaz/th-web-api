# treehouse web app
---
This app work with the treehouse-user API.

1. It makes a request to teamtreehouse.com/\<username\>.json
  where username is any username registered on the site.
2. It process' the data
3. It shows the profile-picture, username, badges and JavaScript points.
